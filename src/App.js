import React from 'react';
import './App.css';
import noCover from './assets/default-nocover.png';

class App extends React.Component{
  constructor(props) {
	super(props);
	this.state = {
	  "search":null,
	  "response_time":0,
	  "currentPage":null
	};
	this.searchBooks = this.searchBooks.bind(this);
  }

  componentDidMount(){
	document.querySelector("#searchText").focus();
  }
  
  render(){

	let searchTable = [];

	if(this.state.searchData != null){
	  let searchItems = this.state.searchData.items;
	  for(let s in searchItems){
		searchTable.push(
		  <div className="book-volume" onClick={this.expandDescription}>
			<div className="book-volume-thumbnail">
			  <img alt="book cover" src={searchItems[s].volumeInfo.imageLinks != null ?
				  searchItems[s].volumeInfo.imageLinks.thumbnail:noCover}/>
			</div>
			<div className="book-volume-authors">
			  {searchItems[s].volumeInfo.authors != null ?
			  	searchItems[s].volumeInfo.authors.join(", "):"Unknown Author"}
			</div>
			<div className="book-volume-title">
			{searchItems[s].volumeInfo.title != null ?
			searchItems[s].volumeInfo.title:"Untitled"}
			</div>
			<div className="book-volume-description hidden">
			  {searchItems[s].volumeInfo.description != null ?
			  searchItems[s].volumeInfo.description:"No description available"}
			</div>
		  </div>
		);
	  }
	}
	
	let pagination = this.state.currentPage===null ? null: 
	<div className="pagination">
		<button className="pagination-button" onClick={()=>{this.setPage(-1)}}>Previous</button>
		<button className="pagination-button" onClick={()=>{this.setPage(1)}}>Next</button>
	</div>;

	return (
		<div className="App">
			<div className="App-header">G-Books Search
				<form className="search-bar" onSubmit={this.searchBooks}>
					<input id="searchText" name="search" type="text"/><button type="submit" className="search-button">Search</button>
				</form>
				
				<div className="search-header">{this.state.searchData!=null? ""+this.state.searchData.totalItems+" items in "+this.state.response_time+"ms":""}
					<br/>{this.state.common_author!=null? "Common Author: "+this.state.common_author:""}
					<br/>{this.state.publish_date_range!=null? "Publish Date Range: "+this.state.publish_date_range:""}
					<br/>{pagination}
					<div className="search-content">
						{searchTable}
					</div>
				</div>
				{pagination}
			</div>
		</div>
	);
  }

  searchBooks = (e) => {
	e.preventDefault();
	this.getBooks(e.target.search.value);
  }

  setPage = (p) => {
	
	let pageNumber = this.state.currentPage+p;
	
	if(pageNumber < 0){pageNumber = 0}
	
	this.getBooks(this.state.search, pageNumber);
  }

  getBooks = async(search,p=null) => {
	
	let searchStart = Date.now();
	
	let searchQ = search.replace(" ", "+");
	let pageQ = p===null ? "": "&startIndex="+10*p;
	let searchJSON = await fetch("https://www.googleapis.com/books/v1/volumes?q="+searchQ+pageQ+"&key=AIzaSyCKmTXd-SfDw2z5woYykcoTq2YctH8YYRA")
							  .then(response => response.json());
	
	
	let items = searchJSON.items;
	let authors = {};
	let dates = [];
	for(let i in items){
		if(items[i].volumeInfo.publishedDate){
			dates.push(items[i].volumeInfo.publishedDate);
		}
		if(items[i].volumeInfo.authors != null){
			if(authors[items[i].volumeInfo.authors[0]]){
				authors[items[i].volumeInfo.authors[0]]++;
			}else{
				authors[items[i].volumeInfo.authors[0]] = 1;
			}
		}
	}

	dates.sort();
	let dateRange = dates[0]+" to "+dates[dates.length-1];

	let commonAuthor = "";
	let maxNumber = 0;
	for(let a in authors){
		if(authors[a]>maxNumber){
			commonAuthor = a;
			maxNumber = authors[a];
		}
	}


	this.setState(Object.assign(this.state, {"search":searchQ, "searchData":searchJSON, 
	"currentPage":p===null?0:p, "response_time":Date.now()-searchStart, "common_author":commonAuthor, "publish_date_range":dateRange}));
  }

  expandDescription(e){
	
	let expandedDescriptionEl = document.querySelector(".book-volume-description:not(.hidden)");
	let descriptionEl = e.currentTarget.querySelector(".book-volume-description");
	
	if(expandedDescriptionEl != null){
		expandedDescriptionEl.classList.add("hidden");
	}
	if(expandedDescriptionEl !== descriptionEl){
		descriptionEl.classList.remove("hidden");
	}
	
  }
}



export default App;
